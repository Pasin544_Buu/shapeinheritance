/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.shapeinheritance;

/**
 *
 * @author Pla
 */
public class Shape {

    protected double h;
    protected double w;

    public void Shape(double h, double w) {
        this.h = h;
        this.w = w;
    }

    public void calArea() {
        System.out.println("Area is " + this.w * this.h);
    }

}
