/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.shapeinheritance;

/**
 *
 * @author Pla
 */
public class Circle extends Shape {

    private double radius;
    private double pi = 22.0 / 7.0;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public void calArea() {

        System.out.println("Circle Area is " + radius * radius * pi);
    }
}
