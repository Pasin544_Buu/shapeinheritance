/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.shapeinheritance;

/**
 *
 * @author Pla
 */
public class Rectangle extends Shape {

    protected double hight;
    protected double width;

    public Rectangle(double hight, double width) {
        this.hight = hight;
        this.width = width;
    }

    @Override
    public void calArea() {
        System.out.println("Rectangle Area is " + hight * width);
    }
}
