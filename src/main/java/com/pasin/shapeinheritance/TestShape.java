/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.shapeinheritance;

/**
 *
 * @author Pla
 */
public class TestShape {

    public static void main(String[] args) {

        Rectangle rectangle = new Rectangle(5, 6);
        rectangle.calArea();

        Triangle triangle = new Triangle(5, 5);
        triangle.calArea();

        Square square = new Square(5);
        square.calArea();

        Circle circle1 = new Circle(7);
        circle1.calArea();

        Circle circle2 = new Circle(14);
        circle2.calArea();

        System.out.println("-----For Loop-----5");
        Shape[] shapes = {rectangle, triangle, square, circle1, circle2};
        for (int i = 0; i < shapes.length; i++) {
            shapes[i].calArea();
        }
    }

}
