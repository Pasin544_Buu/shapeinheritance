/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.shapeinheritance;

/**
 *
 * @author Pla
 */
public class Triangle extends Shape {

    private double base;
    private double hight;

    public Triangle(double base, double hight) {
        this.base = base;
        this.hight = hight;
    }

    @Override
    public void calArea() {
        System.out.println("Triangle Area is " + hight * base * 0.5);
    }
}
